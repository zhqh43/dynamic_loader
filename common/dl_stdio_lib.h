#ifndef DL_STDIO_LIB_H__
#define DL_STDIO_LIB_H__

#include "dl_extern_lib.h"

#include <stdio.h>
#include <stdarg.h>

#define STDIN		0
#define STDOUT		1
#define STDERR		2
#define VFPRINTF	3
#define VFSCANF		4
#define REMOVE		5
#define RENAME		6
#define FCLOSE		7
#define FFLUSH		8
#define FOPEN		9
#define FREOPEN		10
#define FREAD		11
#define FWRITE		12
#define FSEEK		13
#define FTELL		14
#define FGETC		15
#define FGETS		16
#define FPUTC		17
#define FPUTS		18
#define FGETPOS		19
#define FSETPOS		20
#define GETC		21
#define GETS		22
#define PUTC		23
#define PUTS		24

#endif
